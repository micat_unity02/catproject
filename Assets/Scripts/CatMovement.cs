using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 고양이의 이동 기능을 나타내기 위한 컴포넌트입니다.
/// </summary>
public class CatMovement : MonoBehaviour
{
	/// <summary>
	/// 고양이가 이동할 수 있는 땅의 반지름을 나타냅니다.
	/// 이 값은 인스펙터에서 설정됩니다.
	/// </summary>
	public float m_GroundRadius;

	/// <summary>
	/// 고양이의 이동 속력입니다.
	/// </summary>
	public float m_Speed;

	/// <summary>
	/// 목적지를 나타냅니다.
	/// 고양이는 이 위치를 향해 이동합니다.
	/// </summary>
	private Vector3 _Destination;

	/// <summary>
	/// 이동중임을 나타냅니다.
	/// </summary>
	private bool _IsMoving;

	/// <summary>
	/// 도착한 시간을 저장할 변수를 나타냅니다.
	/// </summary>
	private float _GoalTime;

	/// <summary>
	/// 이동을 시작시킬 시간을 나타냅니다.
	/// </summary>
	private float _NextMoveStartTime;

	


	private void Start()
	{
		// 목적지로 이동을 시작합니다.
		StartMoveToDestination();
	}

	private void Update()
	{
		// 이동 상태가 아니며,
		if (!_IsMoving && 
			// 다음 이동 시간이 지난 경우
			(Time.time > _NextMoveStartTime))
		{
			// 새로운 목적지를 설정합니다.
			StartMoveToDestination();
		}

		// 목적지로 이동합니다.
		Move();
	}

	/// <summary>
	/// 목적지로 이동합니다.
	/// </summary>
	private void Move()
	{
		// 목적지까지 남은 거리
		float distance = Vector3.Distance(
			_Destination, transform.position);


		// 목적지까지의 방향을 얻습니다.
		Vector3 direction = _Destination - transform.position;
		direction.y = 0.0f;
		direction.Normalize();

		// 이동시킬 때 사용될 속도를 계산합니다.
		Vector3 velocity = direction * m_Speed * Time.deltaTime;

		// 벡터의 길이를 얻습니다.
		float velocityLength = velocity.magnitude;

		// 남은 거리가 이동될 거리보다 가깝다면
		if (distance < velocityLength)
		{
			// 남은 거리만큼만 이동하도록 합니다.
			velocity = velocity.normalized * distance;
		}

		// velocity 만큼 이동시킵니다.
		transform.position += velocity;

		// 목적지에 도착했다면
		if (velocity.magnitude < 0.0001f)
		{
			if (_IsMoving)
			{
				// 멈춤 상태로 설정합니다.
				_IsMoving = false;

				// 현재 시간을 저장합니다.
				_GoalTime = Time.time;

				// 다음 이동 시작 시간을 결정합니다.
				_NextMoveStartTime = 
					_GoalTime + Random.Range(1.0f, 10.0f);
			}
		}






	}

	/// <summary>
	/// 영역 내부의 랜덤한 위치를 반환합니다.
	/// 랜덤 이동 목표 지점을 설정하기 위하여 사용됩니다.
	/// </summary>
	/// <returns>랜덤한 위치를 반환</returns>
	private Vector3 GetRandomPositionInGround()
	{
		//return new Vector3(
		//	Random.Range(-1.0f, 1.0f),
		//	0.0f,
		//	Random.Range(-1.0f, 1.0f)).normalized * m_GroundRadius;


		// 랜덤한 위치를 뽑습니다.
		float x = Random.Range(-1.0f, 1.0f);
		float y = Random.Range(-1.0f, 1.0f);

		// 랜덤한 위치를 방향으로 만듭니다.
		Vector2 newDirection = new Vector2(x, y);

		// 원 범위를 벗어난 위치가 만들어졌다면
		// 벡터의 길이 : Vector3Data.magnitude
		newDirection = (newDirection.magnitude > 1) ?
			newDirection.normalized : newDirection;
		//if (newDirection.magnitude > 1)
		//{ 
		//	// 벡터의 길이를 1 로 만듭니다.
		//	newDirection.Normalize();
		//}

		// 각 축 값에 반지름을 곱하여 원 내부의 점 위치로 만듭니다.
		newDirection *= m_GroundRadius;

		// 랜덤하게 생성한 값을 3차원 공간 내부에서 사용할 수 있도록 변환하여 반환합니다.
		return new Vector3(newDirection.x, 0.0f, newDirection.y);
	}


	/// <summary>
	/// 랜덤한 위치로 이동을 시작시킵니다.
	/// </summary>
	private void StartMoveToDestination()
	{
		// 랜덤한 목적지를 설정합니다.
		_Destination = GetRandomPositionInGround();

		// 이동중 상태로 변경합니다.
		_IsMoving = true;
	}


	//#if UNITY_ANDROID
	//#if UNITY_IOS
#if UNITY_EDITOR // 유니티 에디터에서만 작동하도록 합니다.
	/// <summary>
	/// 씬에 어떠한 것을 항상 그리도록 합니다.
	/// OnDrawGizmos 메서드는 에디터에서만 사용 가능하며, 전처리문을 통해 빌드 이후에는 작동하지 않도록 해야 합니다.
	/// 오브젝트가 선택되었을 경우에만 그리도록 하려면 OnDrawGizmosSelected 메서드를 정의해야 합니다.
	/// </summary>
	private void OnDrawGizmos()
	{

		// 다음에 그려지게 될 기즈모의 색상을 붉은 색상으로 설정합니다.
		Gizmos.color = Color.red;
		// Color : 색상을 나타내기 위한 형식
		// RGBA 요소가 모두 float 형식입니다.
		// 각각의 요소를 0.0f ~ 1.0f 까지 설정하여 사용 가능
		// Color32 : 색상을 나타내기 위한 형식
		// RGBA 요소가 모두 byte 형식입니다.
		// 각각의 요소를 0.0f ~ 255f 까지 설정하여 사용 가능

		Gizmos.DrawWireSphere(
			Vector3.zero,
			m_GroundRadius);
		// Gizmos : 씬 뷰에서 시각적 디버깅을 위한 클래스입니다.
		// OnDrawGizmos 나 OnDrawGizmosSelected 메서드 내부에서만 사용 가능합니다.
		// DrawWireSphere(center, radius)
		// center 위치에 radius 반지름 만큼의 와이어로 이루어진 구를 그립니다.

		// 선을 from 에서 to 까지 그립니다.
		Gizmos.DrawLine(
			_Destination,
			_Destination + (Vector3.up * 5));
	}
#endif


}
