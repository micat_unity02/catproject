using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour
{
	private void Update()
	{
		transform.position =
			transform.position +
			Vector3.right * 20f * Time.deltaTime;

		// deltaTime
		// 프레임 사이 시간 간격

		// 1 초 144FPS
		// dt : 0.0069444
		// 1프레임마다 오른쪽으로 0.1만큼씩 이동
		// (144 * 0.1) * 오른쪽 = (14.4, 0, 0)
		// (144 * 0.1 * dt) * 오른쪽 = (0.09999936, 0, 0)

		// 1 초 60FPS
		// dt : 0.0166666
		// 1프레임마다 오른쪽으로 0.1만큼씩 이동
		// (60 * 0.1 * dt) * 오른쪽 = (0.0999996, 0, 0)

	}
}

